﻿using ApplicationApp.Interfaces.Generics;
using Entities.Entities;
using Entities.Entities.Inputs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationApp.Interfaces
{
    public interface IFilmeApp : InterfaceGenericaApp<Filme>
    {
        Task<List<Filme>> ListarFilmes(InputFiltroFilme filtro);
    }
}
