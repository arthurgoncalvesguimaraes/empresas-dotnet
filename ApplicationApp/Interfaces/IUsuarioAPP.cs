﻿using ApplicationApp.Interfaces.Generics;
using Entities.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationApp.Interfaces
{
   public interface IUsuarioAPP : InterfaceGenericaApp<Usuario>
    {
        Task<List<Usuario>> ListarUsuariosAtivos();

        Task DeleteUsuario(string id);

        Task<Usuario> GetUsuarioById(string Id);
    }
}
