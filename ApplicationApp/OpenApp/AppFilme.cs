﻿using ApplicationApp.Interfaces;
using Domain.Interfaces.IFilme;
using Entities.Entities;
using Entities.Entities.Inputs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationApp.OpenApp
{
    public class AppFilme : IFilmeApp
    {
        IFilme _IFilme;
        public AppFilme(IFilme IFilme)
        {
            _IFilme = IFilme;
        }
        public async Task Add(Filme Objeto)
        {
            await _IFilme.Add(Objeto);
        }

        public async Task Delete(Filme Objeto)
        {
            await _IFilme.Delete(Objeto);
        }

        public async Task<Filme> GetEntityById(int Id)
        {
            return await _IFilme.GetEntityById(Id);
        }

        public async Task<List<Filme>> List()
        {
            return await _IFilme.List();
        }

        public async Task<List<Filme>> ListarFilmes(InputFiltroFilme filtro)
        {
            return await _IFilme.ListarFilmes(filtro);
        }

        public async Task Update(Filme Objeto)
        {
            await _IFilme.Update(Objeto);
        }
    }
}
