﻿using ApplicationApp.Interfaces;
using ApplicationApp.Interfaces.Generics;
using Domain.Interfaces.IUsuario;
using Entities.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationApp.OpenApp
{
    public class AppUsuario : IUsuarioAPP
    {
        IUsuario _IUsuario;

        public  AppUsuario(IUsuario IUsuario){
            _IUsuario = IUsuario;
    }

        public async Task Add(Usuario Objeto)
        {
           await  _IUsuario.Add(Objeto);
        }

        public async Task Delete(Usuario Objeto)
        {
            await _IUsuario.Update(Objeto);
        }

        public async Task DeleteUsuario(string id)
        {
            await _IUsuario.DeleteUsuario(id);
        }

        public async Task<List<Usuario>> ListarUsuariosAtivos()
        {
            return await _IUsuario.ListarUsuariosAtivos();
        }

        public async Task<Usuario> GetEntityById(int Id)
        {
           return  await _IUsuario.GetEntityById(Id);
        }

        public async Task<List<Usuario>> List()
        {
            return await _IUsuario.List();
        }


        public async Task Update(Usuario Objeto)
        {
             await _IUsuario.Update(Objeto);
        }

        public async Task<Usuario> GetUsuarioById(string Id)
        {
            return await _IUsuario.GetUsuarioById(Id);
        }
    }
}
