﻿using ApplicationApp.Interfaces;
using ApplicationApp.Interfaces.Generics;
using Domain.Interfaces.IUsuario;
using Entities.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationApp.OpenApp
{
    public class AppVoto : IVotoApp
    {
        IVoto _voto;

        public AppVoto(IVoto Ivoto){
            _voto = Ivoto;
      }

        public async Task Add(Voto Objeto)
        {
            _voto.Add(Objeto);
        }

        public async Task AddVoto(Voto voto)
        {
            

            await _voto.AddVoto(voto);
        }

        public Task Delete(Voto Objeto)
        {
            throw new NotImplementedException();
        }

        public Task<Voto> GetEntityById(int Id)
        {
            throw new NotImplementedException();
        }

        public Task<List<Voto>> List()
        {
            throw new NotImplementedException();
        }

        public Task Update(Voto Objeto)
        {
            throw new NotImplementedException();
        }
    }
}
