﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ApplicationApp.Interfaces;
using Entities.Entities;
using Entities.Entities.Inputs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
   
    [Route("api/[controller]")]
    [ApiController]
    public class AdministradoresController : ControllerBase
    {

         private readonly IUsuarioAPP _IUsuarioAPP;
        private readonly UserManager<Usuario> _userManager;

        public AdministradoresController(IUsuarioAPP IUsuarioAPP,
             UserManager<Usuario> UserManager) {

            _IUsuarioAPP = IUsuarioAPP;
            _userManager = UserManager;

        }



        // POST: api/Administradores
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] InputUsuario Usuario)
        {
                var usr = new Usuario()
                { 
                    deleted = false,
                    nome = Usuario.Nome,
                    Email = Usuario.Email,
                    PasswordHash = Usuario.Senha,
                    UserName = Usuario.Email,
                    Tipo = Entities.Entities.Enums.TipoUsuario.Administrador,
                };

           
            var result = await _userManager.CreateAsync(usr, usr.PasswordHash);
            if (!result.Succeeded) return BadRequest(result.Errors);

            await _userManager.AddClaimAsync(usr, new Claim("Filme", "Detalhar,Listar,Editar,Incluir,Excluir"));
            await _userManager.AddClaimAsync(usr, new Claim("Usuario", "Detalhar,Listar,Editar,Incluir,Excluir"));

            return Ok();

        }


        [HttpGet("/api/Administrador")]
        public async Task<IActionResult> Get([FromQuery] string id)
        {
            return Ok(await _IUsuarioAPP.GetUsuarioById(id));
        }


        // PUT: api/Administradores/5
        [Authorize]
        [HttpPut]
        public async Task<IActionResult> Put([FromBody] Usuario Usuario)
        {
            Usuario.Tipo = Entities.Entities.Enums.TipoUsuario.Administrador;
            await _IUsuarioAPP.Update(Usuario);
            return Ok();
        }

        // DELETE: api/ApiWithActions/5
        [Authorize]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            await _IUsuarioAPP.DeleteUsuario(id);
            return Ok();
        }


        [Authorize]
        [ClaimsAuthorize("Usuario", "Listar")]
        [HttpGet("/api/ListUsers")]
        public async Task<IActionResult> ListUsers()
        {
            return Ok(await _IUsuarioAPP.ListarUsuariosAtivos());
        }
    }
}
