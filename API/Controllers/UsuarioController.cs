﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ApplicationApp.Interfaces;
using Entities.Entities;
using Entities.Entities.Inputs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
   
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {

         private readonly IUsuarioAPP _IUsuarioAPP;
        private readonly UserManager<Usuario> _userManager;

        public UsuarioController(IUsuarioAPP IUsuarioAPP,
             UserManager<Usuario> UserManager) {

            _IUsuarioAPP = IUsuarioAPP;
            _userManager = UserManager;

        }

    
      

        // POST: api/Administradores
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] InputUsuario Usuario)
        {
                var usr = new Usuario()
                { 
                    deleted = false,
                    nome = Usuario.Nome,
                    Email = Usuario.Email,
                    PasswordHash = Usuario.Senha,
                    UserName = Usuario.Email,
                    Tipo = Entities.Entities.Enums.TipoUsuario.Comum,
                };

           
            var result = await _userManager.CreateAsync(usr, usr.PasswordHash);
            if (!result.Succeeded) return BadRequest(result.Errors);
            await _userManager.AddClaimAsync(usr, new Claim("Usuario", "Detalhar,Editar,Incluir,Excluir"));
            await _userManager.AddClaimAsync(usr, new Claim("Filme", "Votacao"));
            return Ok();

        }


        [HttpGet("/api/Usuario")]
        public async Task<IActionResult> Get([FromQuery] string id)
        {
            return Ok(await _IUsuarioAPP.GetUsuarioById(id));
        }


        // PUT: api/Administradores/5
        [Authorize]
        [ClaimsAuthorize("Usuario", "Editar")]
        [HttpPut]
        public async Task<IActionResult> Put([FromBody] InputUsuario Usuario)
        {
            var usr = new Usuario()
            {
                deleted = false,
                nome = Usuario.Nome,
                Email = Usuario.Email,
                PasswordHash = Usuario.Senha,
                UserName = Usuario.Email,
                Tipo = Entities.Entities.Enums.TipoUsuario.Comum,
            };

            //sempre confirmar o tipo comum, caso alterem para administrador no front end
            await _IUsuarioAPP.Update(usr);
            return Ok();
        }

        // DELETE: api/ApiWithActions/5
        [Authorize]
        [ClaimsAuthorize("Usuario", "Excluir")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            await _IUsuarioAPP.DeleteUsuario(id);
            return Ok();
        }


    }
}
