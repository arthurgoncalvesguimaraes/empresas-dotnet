﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ApplicationApp.Interfaces;
using Entities.Entities;
using Entities.Entities.Inputs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {

        private readonly IUsuarioAPP _IUsuarioAPP;
        private readonly SignInManager<Usuario> _signInManager;
        private readonly UserManager<Usuario> _userManager;
        private readonly AppSettings _appSettings;

        public AuthController(IUsuarioAPP IUsuarioAPP,
             SignInManager<Usuario> SignInManager,
               UserManager<Usuario> UserManager,
                 IOptions<AppSettings> appSettings
             )
        {

            _IUsuarioAPP = IUsuarioAPP;
            _signInManager = SignInManager;
            _userManager = UserManager;
            _appSettings = appSettings.Value;

        }

        [HttpPost("Login")]
        public async Task<ActionResult> Login(InputUsuario usuario)
        {

            var result = await _signInManager.PasswordSignInAsync(usuario.Email, usuario.Senha, false, true);
            if (result.Succeeded) {
                return Ok(await GerarJwt(usuario.Email));
            }
            return BadRequest("Usuário ou senha inválidos");
        }

        private async Task<string> GerarJwt(string email)
        {
            var user = await _userManager.FindByEmailAsync(email);

            var identityClaims = new ClaimsIdentity();
            identityClaims.AddClaims(await _userManager.GetClaimsAsync(user));

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = identityClaims,

                //Subject = new ClaimsIdentity(new[]
                //{
                //    new Claim(ClaimTypes.Name, user.Id)
                //}),
                Issuer = _appSettings.Emissor,
                Audience = _appSettings.ValidoEm,
                Expires = DateTime.UtcNow.AddHours(_appSettings.ExpiracaoHoras),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            return tokenHandler.WriteToken(tokenHandler.CreateToken(tokenDescriptor));
        }

    }
}