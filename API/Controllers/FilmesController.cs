﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationApp.Interfaces;
using Entities.Entities;
using Entities.Entities.Inputs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{

    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class FilmesController : ControllerBase
    {
        private readonly IFilmeApp _IFilmeApp;
        private readonly IVotoApp _IVotoApp;
        public FilmesController(IFilmeApp IFilmeApp, IVotoApp IVotoApp) {
            _IFilmeApp = IFilmeApp;
            _IVotoApp = IVotoApp;

        }

        [HttpGet("/api/Filme")]
        public async Task<IActionResult> Get([FromQuery] int id)
        {
            return Ok(await _IFilmeApp.GetEntityById(id));
        }

        [ClaimsAuthorize("Filme", "Incluir")]
        // POST: api/Filmes
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Filme filme)
        {
             await _IFilmeApp.Add(filme);
            return Ok();
        }


        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] InputFiltroFilme filtro)
        {
            return Ok(await _IFilmeApp.ListarFilmes(filtro));
        }

        // POST: api/Filmes
        [ClaimsAuthorize("Filme", "Votacao")]
        [HttpPost("api/Votacao")]
        public async Task<IActionResult> Votacao([FromBody] InputVotacao voto)
        {

            if (voto.valor > 4)
            {
                return BadRequest("Campo Valor incorreto");
            }

            var votacao = new Voto() { 
                idFilme = voto.idFilme,
                Valor = voto.valor,
                idUsuario = voto.idUsuario
            };

            await _IVotoApp.AddVoto(votacao);
            return Ok();
        }

        

    }
}
