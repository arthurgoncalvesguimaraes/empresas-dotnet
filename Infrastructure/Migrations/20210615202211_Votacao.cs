﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Migrations
{
    public partial class Votacao : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Voto_Filme_idUsuario",
                table: "Voto");

            migrationBuilder.AlterColumn<string>(
                name: "idUsuario",
                table: "Voto",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<int>(
                name: "idFilme",
                table: "Voto",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Voto_idFilme",
                table: "Voto",
                column: "idFilme");

            migrationBuilder.AddForeignKey(
                name: "FK_Voto_AspNetUsers_idUsuario",
                table: "Voto",
                column: "idUsuario",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Voto_Filme_idFilme",
                table: "Voto",
                column: "idFilme",
                principalTable: "Filme",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Voto_AspNetUsers_idUsuario",
                table: "Voto");

            migrationBuilder.DropForeignKey(
                name: "FK_Voto_Filme_idFilme",
                table: "Voto");

            migrationBuilder.DropIndex(
                name: "IX_Voto_idFilme",
                table: "Voto");

            migrationBuilder.DropColumn(
                name: "idFilme",
                table: "Voto");

            migrationBuilder.AlterColumn<int>(
                name: "idUsuario",
                table: "Voto",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Voto_Filme_idUsuario",
                table: "Voto",
                column: "idUsuario",
                principalTable: "Filme",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
