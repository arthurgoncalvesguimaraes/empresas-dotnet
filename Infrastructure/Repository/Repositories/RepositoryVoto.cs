﻿using Domain.Interfaces.IUsuario;
using Entities.Entities;
using Infrastructure.Configuration;
using Infrastructure.Repository.Generics;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace Infrastructure.Repository.Repositories
{
    public class RepositoryVoto : RepositoryGenerics<Voto>, IVoto
    {

        private readonly DbContextOptions<ContextBase> _optionsBuilder;

        public RepositoryVoto()
        {
            _optionsBuilder = new DbContextOptions<ContextBase>();

        }

        public async Task AddVoto(Voto voto)
        {
            using (var data = new ContextBase(_optionsBuilder))
            {
                await data.Set<Voto>().AddAsync(voto);

                var filme = await data.Set<Filme>().FindAsync(voto.idFilme);

                var qtdVotos = data.Set<Voto>().Where(x => x.idFilme == voto.idFilme).Count() + 1;
                filme.mediaVotos = (filme.mediaVotos + voto.Valor) / qtdVotos;
                filme.qtdVotos = qtdVotos;
                data.Set<Filme>().Update(filme);

                await data.SaveChangesAsync();
            }
        }
    }
}
