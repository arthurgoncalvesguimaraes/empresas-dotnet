﻿
using Entities.Entities;
using Infrastructure.Configuration;
using Infrastructure.Repository.Generics;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Domain.Interfaces.IFilme;
using Entities.Entities.Inputs;

namespace Infrastructure.Repository.Repositories
{
    public class RepositoryFilme : RepositoryGenerics<Filme>, IFilme
    {
        private readonly DbContextOptions<ContextBase> _optionsBuilder;

        public RepositoryFilme()
        {
            _optionsBuilder = new DbContextOptions<ContextBase>();

        }

        public async Task<List<Filme>> ListarFilmes(InputFiltroFilme filtro)
        {
            using (var data = new ContextBase(_optionsBuilder))
            {
                return await data.Filme.Where(x =>

                 filtro.diretor != null ? x.diretor.Contains(filtro.diretor) : true &&
                 filtro.nome != null ? x.nome.Contains(filtro.nome) : true &&
                 filtro.nome != null ? x.genero.Contains(filtro.genero) : true &&
                 filtro.nome != null ? x.atores.Contains(filtro.ator) : true

                ).OrderByDescending(x=> x.mediaVotos).OrderBy(f=> f.nome).ToListAsync();

            }
        }

    }
}
