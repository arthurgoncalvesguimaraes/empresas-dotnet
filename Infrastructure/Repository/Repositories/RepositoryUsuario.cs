﻿using Domain.Interfaces.IUsuario;
using Entities.Entities;
using Infrastructure.Configuration;
using Infrastructure.Repository.Generics;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Entities.Entities.Enums;

namespace Infrastructure.Repository.Repositories
{
    public class RepositoryUsuario : RepositoryGenerics<Usuario>, IUsuario
    {
        private readonly DbContextOptions<ContextBase> _optionsBuilder;

        public RepositoryUsuario()
        {
            _optionsBuilder = new DbContextOptions<ContextBase>();

        }
        public async Task<List<Usuario>> ListarUsuariosAtivos()
        {
            using (var data = new ContextBase(_optionsBuilder))
            {
                return await data.Usuario.Where(x => x.deleted == false && x.Tipo == TipoUsuario.Comum).ToListAsync();

            }
        }


        public async Task DeleteUsuario(string id)
        {
            using (var data = new ContextBase(_optionsBuilder))
            {
                var obj = await data.Set<Usuario>().FindAsync(id);
                obj.deleted = true;
                data.Set<Usuario>().Update(obj);
                await data.SaveChangesAsync();
            }
        }

        public async Task<Usuario> GetUsuarioById(string Id)
        {
            using (var data = new ContextBase(_optionsBuilder))
            {
                var usuario = await data.Set<Usuario>().FindAsync(Id);
                usuario.PasswordHash = "";
                return usuario;
            }
        }



    }
}
