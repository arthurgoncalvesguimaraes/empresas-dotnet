﻿using Entities.Entities;
using Entities.Entities.Enums;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Configuration
{
    public class ContextBase : IdentityDbContext<Usuario>
    {
        public ContextBase(DbContextOptions<ContextBase> options) : base(options)
        {
            //Database.EnsureCreated();

        }

        public DbSet<Filme> Filme { get; set; }
        public DbSet<Usuario> Usuario { get; set; }
        public DbSet<Voto> Voto { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
                optionsBuilder.UseSqlServer(GetStringConectionConfig());
            base.OnConfiguring(optionsBuilder);
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
          
            base.OnModelCreating(builder);
        }
        private string GetStringConectionConfig()
        {
            string strCon = "data source=184.168.194.64;initial catalog=DbIOSyst;persist security info=True;user id=usrDev;password=c6F5fb^7;MultipleActiveResultSets=True;App=EntityFramework";
            return strCon;
        }
    }
}
