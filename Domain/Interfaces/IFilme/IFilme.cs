﻿using Domain.Interfaces.Generics;
using Entities.Entities;
using Entities.Entities.Inputs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces.IFilme
{
    public interface IFilme : IGeneric<Filme>
    {
        Task<List<Filme>> ListarFilmes(InputFiltroFilme filtro);
    }
}
