﻿using Domain.Interfaces.Generics;
using Entities.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces.IUsuario
{
    public interface IUsuario : IGeneric<Usuario>
    {
        Task<List<Usuario>> ListarUsuariosAtivos();

        Task DeleteUsuario(string id);

        Task<Usuario> GetUsuarioById(string Id);
    }
}
