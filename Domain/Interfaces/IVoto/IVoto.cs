﻿using Domain.Interfaces.Generics;
using Entities.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces.IUsuario
{
    public interface IVoto : IGeneric<Voto>
    {
        Task AddVoto(Voto voto);
    }
}
