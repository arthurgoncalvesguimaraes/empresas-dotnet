﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Entities.Entities
{
    public class Filme : Base
    {
        [Display(Name = "Nome")]
        public string diretor { get; set; }

        [Display(Name = "Gênero")]
        public string genero { get; set; }

        [Display(Name = "Atores")]
        public string  atores { get; set; }

        [Display(Name = "Qtd Votos")]
        public int qtdVotos { get; set; }

        [Display(Name = "Média de Votos")]
        public float mediaVotos { get; set; }

       

    }
}
