﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Entities.Inputs
{
    public class InputFiltroFilme
    {
        public string diretor { get; set; }
        public string nome { get; set; }
        public string genero { get; set; }
        public string ator { get; set; }
    }
}
