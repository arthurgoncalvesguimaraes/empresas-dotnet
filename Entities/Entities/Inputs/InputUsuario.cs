﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Entities.Inputs
{
    public class InputUsuario
    {
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
    }
}
