﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Entities.Inputs
{
    public class InputVotacao
    {
        public int valor { get; set; }
        public int idFilme { get; set; }
        public string idUsuario { get; set; }
    }
}
