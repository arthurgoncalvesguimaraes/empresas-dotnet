﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Entities.Entities
{
    public class Base
    {
        [Display(Name = "Código")]
        public int id { get; set; }
        [Display(Name = "Nome")]
        public string nome { get; set; }
    }
}
