﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;
using System.Text;
using Entities.Entities.Enums;

namespace Entities.Entities
{
    public class Voto : Base
    {
        [Display(Name = "Valor")]
        public int Valor { get; set; }

        [ForeignKey("Filme")]
        public int idFilme { get; set; }

        [ForeignKey("Usuario")]
        public string idUsuario { get; set; }
        public Filme Filme { get; set; }
        public Usuario Usuario { get; set; }
    }
}
