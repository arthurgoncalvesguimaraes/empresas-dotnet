﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;
using System.Text;
using Entities.Entities.Enums;

namespace Entities.Entities
{
    public class Usuario : IdentityUser
    {
        [Display(Name = "Nome")]
        public string nome { get; set; }

       
        [Display(Name = "Tipo")]
        public TipoUsuario? Tipo { get; set; }

        [Display(Name = "Excluído")]
        public bool deleted { get; set; }
    }
}
